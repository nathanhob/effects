package org.realms.effects.particals;

import org.bukkit.event.inventory.*;
import org.bukkit.entity.*;
import org.bukkit.scheduler.*;
import org.realms.effects.EffectsManager;
import org.bukkit.*;
import org.bukkit.inventory.*;
import org.bukkit.event.*;

public class Flames implements Listener
{
    @EventHandler
    public void onClick(final InventoryClickEvent e) {
        final Player p = (Player)e.getWhoClicked();
        final Inventory inv = e.getInventory();
        if (p.hasPermission("effect.flames.use") && inv.getTitle().contains(ChatColor.translateAlternateColorCodes('&', "&4&lParticle Menu"))) {
            e.setCancelled(true);
            if (e.getCurrentItem() != null && e.getCurrentItem().hasItemMeta() && e.getCurrentItem().getType() == Material.BLAZE_POWDER) {
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', EffectsManager.pl.getConfig().getString("FlamesParticleMessage")));
                if (EffectsManager.timers.containsKey(p.getUniqueId())) {
                    Bukkit.getServer().getScheduler().cancelTask((int)EffectsManager.timers.get(p.getUniqueId()));
                }
                @SuppressWarnings("deprecation")
				final int flame = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(EffectsManager.pl, (BukkitRunnable)new BukkitRunnable() {
                    double t = 1.0;
                    
                    public void run() {
                        final double x = this.t * Math.cos(this.t);
                        final double y = this.t * this.t;
                        final double z = this.t * Math.sin(this.t);
                        p.getLocation().add(x, y, z);
                        p.getWorld().spigot().playEffect(p.getLocation(), Effect.FLAME, 26, 0, 0.0f, 0.0f, 0.0f, 0.0f, 2, 387);
                    }
                }, 0L, 0L);
                EffectsManager.timers.put(p.getUniqueId(), flame);
            }
        }
    }
}
