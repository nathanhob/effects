package org.realms.effects.particals;

import org.bukkit.event.inventory.*;
import org.bukkit.entity.*;
import org.bukkit.scheduler.*;
import org.realms.effects.EffectsManager;
import org.bukkit.*;
import org.bukkit.inventory.*;
import org.bukkit.event.*;

public class Cupid implements Listener
{
    @SuppressWarnings("deprecation")
	@EventHandler
    public void onClick(final InventoryClickEvent e) {
        final Player p = (Player)e.getWhoClicked();
        final Inventory inv = e.getInventory();
        if (p.hasPermission("effect.cupid.use") && inv.getTitle().contains(ChatColor.translateAlternateColorCodes('&', "&4&lParticle Menu"))) {
            e.setCancelled(true);
            if (e.getCurrentItem() != null && e.getCurrentItem().hasItemMeta() && e.getCurrentItem().getType() == Material.EMERALD) {
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', EffectsManager.pl.getConfig().getString("CupidParticleMessage")));
                if (EffectsManager.timers.containsKey(p.getUniqueId())) {
                    Bukkit.getServer().getScheduler().cancelTask((int)EffectsManager.timers.get(p.getUniqueId()));
                }
				final int heart = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(EffectsManager.pl, (BukkitRunnable)new BukkitRunnable() {
                    double t = 1.0;
                    
                    public void run() {
                        final double x = this.t * 2.0 + Math.cos(this.t);
                        final double y = this.t * this.t;
                        final double z = this.t * 2.0 + Math.sin(this.t);
                        p.getWorld().spigot().playEffect(p.getLocation(), Effect.HEART, 26, 0, (float)x, (float)y, (float)z, 5.0f, 10, 387);
                    }
                }, 0L, 0L);
                EffectsManager.timers.put(p.getUniqueId(), heart);
            }
        }
    }
}
