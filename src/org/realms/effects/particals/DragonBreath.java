package org.realms.effects.particals;

import org.bukkit.event.inventory.*;
import org.bukkit.entity.*;
import org.bukkit.scheduler.*;
import org.bukkit.*;
import org.bukkit.util.*;
import org.realms.effects.EffectsManager;
import org.bukkit.inventory.*;
import org.bukkit.event.*;

public class DragonBreath implements Listener
{
    @SuppressWarnings("deprecation")
	@EventHandler
    public void onClick(final InventoryClickEvent e) {
        final Player p = (Player)e.getWhoClicked();
        final Inventory inv = e.getInventory();
        if (p.hasPermission("effect.dragonbreath.use") && inv.getTitle().contains(ChatColor.translateAlternateColorCodes('&', "&4&lParticle Menu"))) {
            e.setCancelled(true);
            if (e.getCurrentItem() != null && e.getCurrentItem().hasItemMeta() && e.getCurrentItem().getType() == Material.BLAZE_ROD) {
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', EffectsManager.pl.getConfig().getString("DragonBreathParticleMessage")));
                if (EffectsManager.timers.containsKey(p.getUniqueId())) {
                    Bukkit.getServer().getScheduler().cancelTask((int)EffectsManager.timers.get(p.getUniqueId()));
                }
                final int dragonbreath = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(EffectsManager.pl, (BukkitRunnable)new BukkitRunnable() {
                    double t = 0.0;
                    
                    public void run() {
                        this.t += 0.5;
                        for (int i = 1; i < 10; ++i) {
                            final Location loc = p.getLocation();
                            final Vector direction = loc.getDirection().normalize();
                            loc.getDirection().lengthSquared();
                            final double x = direction.getX() * this.t;
                            double y = direction.getY() * this.t + 1.5;
                            final double z = direction.getZ() * this.t;
                            loc.add(x, y, z);
                            p.getWorld().spigot().playEffect(loc, Effect.FLAME, 26, 0, 0.0f, 0.0f, 0.0f, 0.0f, 0, 287);
                            p.getWorld().spigot().playEffect(loc, Effect.FLAME, 26, 0, 0.0f, 0.0f, 0.0f, 0.0f, 0, 287);
                            loc.subtract(x, y, z);
                            if (this.t > 30.0) {
                                this.t = 0.0;
                                y = 30.0;
                            }
                        }
                    }
                }, 0L, 0L);
                EffectsManager.timers.put(p.getUniqueId(), dragonbreath);
            }
        }
    }
}
