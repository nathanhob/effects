package org.realms.effects.particals;

import org.bukkit.event.inventory.*;
import org.bukkit.entity.*;
import org.bukkit.scheduler.*;
import org.realms.effects.EffectsManager;
import org.bukkit.*;
import org.bukkit.inventory.*;
import org.bukkit.event.*;

public class RainyCloud implements Listener
{
    @EventHandler
    public void onClick(final InventoryClickEvent e) {
        final Player p = (Player)e.getWhoClicked();
        final Inventory inv = e.getInventory();
        if (p.hasPermission("effect.rainycloud.use") && inv.getTitle().contains(ChatColor.translateAlternateColorCodes('&', "&4&lParticle Menu"))) {
            e.setCancelled(true);
            if (e.getCurrentItem() != null && e.getCurrentItem().hasItemMeta() && e.getCurrentItem().getType() == Material.BROWN_MUSHROOM) {
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&3&lRainy Cloud Enabled"));
                if (EffectsManager.timers.containsKey(p.getUniqueId())) {
                    Bukkit.getServer().getScheduler().cancelTask((int)EffectsManager.timers.get(p.getUniqueId()));
                }
                @SuppressWarnings("deprecation")
				final int rainycloud = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(EffectsManager.pl, (BukkitRunnable)new BukkitRunnable() {
                    double t = 1.0;
                    
                    public void run() {
                        final double x = this.t * Math.cos(this.t);
                        final double y = this.t * this.t;
                        final double z = this.t * Math.sin(this.t);
                        p.getLocation().add(x, y, z);
                        p.getWorld().spigot().playEffect(p.getLocation().add(0.0, 3.0, 0.0), Effect.CLOUD, 26, 0, 0.3f, 0.1f, 0.3f, 0.0f, 20, 387);
                        p.getWorld().spigot().playEffect(p.getLocation().add(0.0, 2.3, 0.0), Effect.SPLASH, 26, 0, 0.1f, 0.05f, 0.1f, 0.0f, 5, 387);
                    }
                }, 0L, 0L);
                EffectsManager.timers.put(p.getUniqueId(), rainycloud);
            }
        }
    }
}
