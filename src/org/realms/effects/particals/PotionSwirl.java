package org.realms.effects.particals;

import org.bukkit.event.inventory.*;
import org.bukkit.entity.*;
import org.bukkit.scheduler.*;
import org.realms.effects.EffectsManager;
import org.bukkit.*;
import java.util.*;
import org.bukkit.inventory.*;
import org.bukkit.event.*;

public class PotionSwirl implements Listener
{
    @EventHandler
    public void onClick(final InventoryClickEvent e) {
        final Player p = (Player)e.getWhoClicked();
        final Inventory inv = e.getInventory();
        if (p.hasPermission("effect.potionswirl.use") && inv.getTitle().contains(ChatColor.translateAlternateColorCodes('&', "&4&lParticle Menu"))) {
            e.setCancelled(true);
            if (e.getCurrentItem() != null && e.getCurrentItem().hasItemMeta() && e.getCurrentItem().getType() == Material.FERMENTED_SPIDER_EYE) {
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', EffectsManager.pl.getConfig().getString("PotionSwirlParticleMessage")));
                if (EffectsManager.timers.containsKey(p.getUniqueId())) {
                    Bukkit.getServer().getScheduler().cancelTask((int)EffectsManager.timers.get(p.getUniqueId()));
                }
                @SuppressWarnings("deprecation")
				final int potionswirl = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(EffectsManager.pl, (BukkitRunnable)new BukkitRunnable() {
                    double t = 0.0;
                    double r = 0.5;
                    
                    public void run() {
                        final Location loc = p.getLocation();
                        this.t += 0.19634954084936207;
                        final double x = this.r * Math.cos(this.t);
                        double y = 0.1 * this.t;
                        final double z = this.r * Math.sin(this.t);
                        loc.add(x, y, z);
                        for (final Player all : Bukkit.getOnlinePlayers()) {
                            all.spigot().playEffect(loc, Effect.POTION_SWIRL_TRANSPARENT, 10, 0, 0.0f, 0.0f, 0.0f, 0.0f, 2, 287);
                        }
                        loc.subtract(x, y, z);
                        if (this.t > 18.84955592153876) {
                            this.t = 0.0;
                            y = 0.0;
                        }
                    }
                }, 0L, 1L);
                EffectsManager.timers.put(p.getUniqueId(), potionswirl);
            }
        }
    }
}
