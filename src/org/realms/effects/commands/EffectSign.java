package org.realms.effects.commands;

import org.bukkit.command.*;
import org.bukkit.entity.*;
import org.bukkit.*;
import org.bukkit.inventory.*;
import org.realms.effects.EffectsManager;

public class EffectSign implements CommandExecutor
{
    public boolean onCommand(final CommandSender s, final Command cmd, final String label, final String[] args) {
        if (s instanceof Player && cmd.getName().equalsIgnoreCase("effectsign")) {
            if (s.hasPermission("effectsign.use")) {
                final ItemStack item = EffectsManager.createItem(Material.SIGN, ChatColor.RED + "Effect Sign");
                ((Player)s).getInventory().setItem(1, item);
                s.sendMessage(ChatColor.YELLOW + "You have recieved a sign");
            }
            else {
                s.sendMessage(ChatColor.RED + "You do not have enough perms");
            }
        }
        return true;
    }
}
