package org.realms.effects.commands;

import org.bukkit.command.*;
import org.bukkit.entity.*;
import org.realms.effects.EffectsManager;
import org.bukkit.*;

public class EffectHelp implements CommandExecutor
{
    public boolean onCommand(final CommandSender s, final Command cmd, final String label, final String[] args) {
        if (s instanceof Player && cmd.getName().equalsIgnoreCase("effecthelp")) {
            final Player p = (Player)s;
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', EffectsManager.pl.getConfig().getString("EffectHelpList1")));
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', EffectsManager.pl.getConfig().getString("EffectHelpList2")));
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', EffectsManager.pl.getConfig().getString("EffectHelpList3")));
        }
        return true;
    }
}
