package org.realms.effects.listeners;

import org.bukkit.event.player.*;
import org.realms.effects.EffectsManager;
import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.event.*;

public class Quit implements Listener
{
    @EventHandler
    public void onQuite(final PlayerQuitEvent e) {
        final Player p = e.getPlayer();
        if (EffectsManager.timers.containsKey(p.getUniqueId())) {
            Bukkit.getServer().getScheduler().cancelTask((int)EffectsManager.timers.get(p.getUniqueId()));
        }
    }
}
