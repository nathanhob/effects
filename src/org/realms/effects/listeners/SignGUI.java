package org.realms.effects.listeners;

import org.bukkit.entity.*;
import org.bukkit.event.*;
import org.bukkit.event.player.*;
import org.bukkit.event.block.*;
import org.bukkit.block.*;
import org.bukkit.inventory.*;
import org.realms.effects.EffectsManager;
import org.bukkit.*;

public class SignGUI implements Listener
{
    private Inventory inv;
    private ItemStack item;
    private ItemStack pane;
    
    @EventHandler
    public void onSignChange(final SignChangeEvent e) {
        final Player p = e.getPlayer();
        if (!p.hasPermission("effectmenu.sign")) {
            e.setCancelled(true);
            if (ChatColor.stripColor(e.getLine(0)).equalsIgnoreCase(EffectsManager.pl.getConfig().getString(ChatColor.translateAlternateColorCodes('&', "Sign")))) {
                e.setLine(0, EffectsManager.pl.getConfig().getString("Sign"));
                e.setLine(1, ChatColor.translateAlternateColorCodes('&', "&4&lClick Here"));
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&a&lSign has been successfully made"));
        }
        }
    }

    
    @EventHandler
    public void onPlayerInteract(final PlayerInteractEvent e) {
        final Player p = e.getPlayer();
        if (e.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return;
        }
        if (e.getClickedBlock().getState() instanceof Sign) {
            final Sign s = (Sign)e.getClickedBlock().getState();
            if (ChatColor.stripColor(s.getLine(0)).equalsIgnoreCase(EffectsManager.pl.getConfig().getString("Sign"))) {
                this.inv = Bukkit.getServer().createInventory((InventoryHolder)null, 63, ChatColor.translateAlternateColorCodes('&', "&4&lParticle Menu"));
                this.item = EffectsManager.createItem(Material.BLAZE_POWDER, ChatColor.translateAlternateColorCodes('&', "&6&lFlames"));
                this.inv.setItem(0, this.item);
                this.item = EffectsManager.createItem(Material.WATER_BUCKET, ChatColor.translateAlternateColorCodes('&', "&3&lRain"));
                this.inv.setItem(2, this.item);
                this.item = EffectsManager.createItem(Material.EYE_OF_ENDER, ChatColor.translateAlternateColorCodes('&', "&d&lEnder Magic"));
                this.inv.setItem(4, this.item);
                this.item = EffectsManager.createItem(Material.EMERALD, ChatColor.translateAlternateColorCodes('&', "&c&lCupid"));
                this.inv.setItem(6, this.item);
                this.item = EffectsManager.createItem(Material.JUKEBOX, ChatColor.translateAlternateColorCodes('&', "&a&lMusic Note"));
                this.inv.setItem(8, this.item);
                this.item = EffectsManager.createItem(Material.FIREWORK, ChatColor.translateAlternateColorCodes('&', "&f&lSparky Rings "));
                this.inv.setItem(10, this.item);
                this.item = EffectsManager.createItem(Material.LAVA_BUCKET, ChatColor.translateAlternateColorCodes('&', "&6&lVolcano "));
                this.inv.setItem(12, this.item);
                this.item = EffectsManager.createItem(Material.BLAZE_ROD, ChatColor.translateAlternateColorCodes('&', "&6&lDragon Breath "));
                this.inv.setItem(14, this.item);
                this.item = EffectsManager.createItem(Material.SAPLING, ChatColor.translateAlternateColorCodes('&', "&a&lGreen Rings "));
                this.inv.setItem(16, this.item);
                this.item = EffectsManager.createItem(Material.TNT, ChatColor.translateAlternateColorCodes('&', "&7&lExplosion"));
                this.inv.setItem(18, this.item);
                this.item = EffectsManager.createItem(Material.SLIME_BALL, ChatColor.translateAlternateColorCodes('&', "&a&lSlime"));
                this.inv.setItem(20, this.item);
                this.item = EffectsManager.createItem(Material.SNOW_BALL, ChatColor.translateAlternateColorCodes('&', "&3&lSnow"));
                this.inv.setItem(22, this.item);
                this.item = EffectsManager.createItem(Material.BOOK, ChatColor.translateAlternateColorCodes('&', "&f&lFlying Glyph"));
                this.inv.setItem(24, this.item);
                this.item = EffectsManager.createItem(Material.RED_MUSHROOM, ChatColor.translateAlternateColorCodes('&', "&4&lR&6&la&e&li&a&ln&b&lb&9&lo&d&lw"));
                this.inv.setItem(26, this.item);
                this.item = EffectsManager.createItem(Material.FLINT_AND_STEEL, ChatColor.translateAlternateColorCodes('&', "&6&lSmoke Cloud"));
                this.inv.setItem(28, this.item);
                this.item = EffectsManager.createItem(Material.WEB, ChatColor.translateAlternateColorCodes('&', "&f&lCloud"));
                this.inv.setItem(30, this.item);
                this.item = EffectsManager.createItem(Material.BROWN_MUSHROOM, ChatColor.translateAlternateColorCodes('&', "&3&lRainy Cloud"));
                this.inv.setItem(32, this.item);
                this.item = EffectsManager.createItem(Material.SNOW_BLOCK, ChatColor.translateAlternateColorCodes('&', "&f&lSnowy Cloud"));
                this.inv.setItem(34, this.item);
                this.item = EffectsManager.createItem(Material.COAL, ChatColor.translateAlternateColorCodes('&', "&8&lShadow"));
                this.inv.setItem(36, this.item);
                this.item = EffectsManager.createItem(Material.FERMENTED_SPIDER_EYE, ChatColor.translateAlternateColorCodes('&', "&7&lPotion Swirl"));
                this.inv.setItem(38, this.item);
                this.item = EffectsManager.createItem(Material.FLINT, ChatColor.translateAlternateColorCodes('&', "&6&lFlame Rings"));
                this.inv.setItem(40, this.item);
                this.item = EffectsManager.createItem(Material.STICK, ChatColor.translateAlternateColorCodes('&', "&3&lSpell"));
                this.inv.setItem(42, this.item);
                this.item = EffectsManager.createItem(Material.DIAMOND_SWORD, ChatColor.translateAlternateColorCodes('&', "&c&lCritz"));
                this.inv.setItem(44, this.item);
                this.item = EffectsManager.createItem(Material.FIREBALL, ChatColor.translateAlternateColorCodes('&', "&9&lThunder Cloud"));
                this.inv.setItem(46, this.item);
                this.item = EffectsManager.createItem(Material.DRAGON_EGG, ChatColor.translateAlternateColorCodes('&', "&5&lM&f&la&5&lg&f&li&5&lc &5&lH&f&le&5&ll&f&li&5&lx"));
                this.inv.setItem(48, this.item);
                this.item = EffectsManager.createItem(Material.NETHERRACK, ChatColor.translateAlternateColorCodes('&', "&6&lLava Drip"));
                this.inv.setItem(50, this.item);
                this.item = EffectsManager.createItem(Material.BEDROCK, ChatColor.translateAlternateColorCodes('&', "&8&lVoid Fog"));
                this.inv.setItem(52, this.item);
                this.item = EffectsManager.createItem(Material.IRON_DOOR, ChatColor.translateAlternateColorCodes('&', "&4&lExit"));
                this.inv.setItem(54, this.item);
                this.item = EffectsManager.createItem(Material.BARRIER, ChatColor.translateAlternateColorCodes('&', "&4&lClear Particle Effects "));
                this.inv.setItem(58, this.item);
                this.pane = EffectsManager.glassPane(Material.STAINED_GLASS_PANE, 1, (byte)14, ChatColor.translateAlternateColorCodes('&', "&4&lGlass"));
                this.inv.setItem(55, this.pane);
                this.inv.setItem(56, this.pane);
                this.inv.setItem(57, this.pane);
                this.inv.setItem(59, this.pane);
                this.inv.setItem(60, this.pane);
                this.inv.setItem(61, this.pane);
                this.inv.setItem(62, this.pane);
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6&lParticle Menu has been opened"));
                p.openInventory(this.inv);
            }
        }
    }
    }