package org.realms.effects.listeners;

import org.bukkit.event.inventory.*;
import org.bukkit.entity.*;
import org.bukkit.*;
import org.bukkit.inventory.*;
import org.bukkit.event.*;

public class ExitMenu implements Listener
{
    @EventHandler
    public void onExit(final InventoryClickEvent e) {
        final Player p = (Player)e.getWhoClicked();
        final Inventory inv = e.getInventory();
        if (inv.getTitle().contains(ChatColor.translateAlternateColorCodes('&', "&4&lParticle Menu"))) {
            e.setCancelled(true);
            if (e.getCurrentItem() != null && e.getCurrentItem().hasItemMeta() && e.getCurrentItem().getType() == Material.IRON_DOOR) {
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&lYou have exited the Effet Menu"));
                p.closeInventory();
            }
        }
    }
}
