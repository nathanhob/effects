package org.realms.effects;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandExecutor;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.realms.effects.commands.EffectCommand;
import org.realms.effects.commands.EffectHelp;
import org.realms.effects.commands.EffectSign;
import org.realms.effects.listeners.CancelParticle;
import org.realms.effects.listeners.ExitMenu;
import org.realms.effects.listeners.Quit;
import org.realms.effects.listeners.SignGUI;
import org.realms.effects.particals.Cloud;
import org.realms.effects.particals.Critz;
import org.realms.effects.particals.Cupid;
import org.realms.effects.particals.DragonBreath;
import org.realms.effects.particals.EnderMagic;
import org.realms.effects.particals.Explosion;
import org.realms.effects.particals.FlameRings;
import org.realms.effects.particals.Flames;
import org.realms.effects.particals.FlyingGlyph;
import org.realms.effects.particals.GreenRings;
import org.realms.effects.particals.LavaDrip;
import org.realms.effects.particals.MagicHelix;
import org.realms.effects.particals.Music;
import org.realms.effects.particals.PotionSwirl;
import org.realms.effects.particals.Rain;
import org.realms.effects.particals.Rainbow;
import org.realms.effects.particals.RainyCloud;
import org.realms.effects.particals.Shadow;
import org.realms.effects.particals.Slime;
import org.realms.effects.particals.SmokeCloud;
import org.realms.effects.particals.Snow;
import org.realms.effects.particals.SnowyCloud;
import org.realms.effects.particals.SparkyRing;
import org.realms.effects.particals.Spell;
import org.realms.effects.particals.ThunderCloud;
import org.realms.effects.particals.VoidFog;
import org.realms.effects.particals.Volcano;


public class EffectsManager extends JavaPlugin
{
    public static HashMap<UUID, Integer> timers;
    public static Plugin pl;
    
    static {
        EffectsManager.timers = new HashMap<UUID, Integer>();
    }
    
    public void onEnable() {
        this.registerListeners();
        this.registerCommands();
        this.registerConfig();
        EffectsManager.pl = (Plugin)this;
    }
    
    private void registerListeners() {
        final PluginManager pm = Bukkit.getServer().getPluginManager();
        pm.registerEvents((Listener)new Flames(), (Plugin)this);
        pm.registerEvents((Listener)new Rain(), (Plugin)this);
        pm.registerEvents((Listener)new EnderMagic(), (Plugin)this);
        pm.registerEvents((Listener)new Music(), (Plugin)this);
        pm.registerEvents((Listener)new Cupid(), (Plugin)this);
        pm.registerEvents((Listener)new SparkyRing(), (Plugin)this);
        pm.registerEvents((Listener)new Volcano(), (Plugin)this);
        pm.registerEvents((Listener)new DragonBreath(), (Plugin)this);
        pm.registerEvents((Listener)new GreenRings(), (Plugin)this);
        pm.registerEvents((Listener)new Explosion(), (Plugin)this);
        pm.registerEvents((Listener)new Slime(), (Plugin)this);
        pm.registerEvents((Listener)new Snow(), (Plugin)this);
        pm.registerEvents((Listener)new FlyingGlyph(), (Plugin)this);
        pm.registerEvents((Listener)new Rainbow(), (Plugin)this);
        pm.registerEvents((Listener)new SmokeCloud(), (Plugin)this);
        pm.registerEvents((Listener)new Cloud(), (Plugin)this);
        pm.registerEvents((Listener)new RainyCloud(), (Plugin)this);
        pm.registerEvents((Listener)new SnowyCloud(), (Plugin)this);
        pm.registerEvents((Listener)new Shadow(), (Plugin)this);
        pm.registerEvents((Listener)new PotionSwirl(), (Plugin)this);
        pm.registerEvents((Listener)new FlameRings(), (Plugin)this);
        pm.registerEvents((Listener)new Spell(), (Plugin)this);
        pm.registerEvents((Listener)new Critz(), (Plugin)this);
        pm.registerEvents((Listener)new ThunderCloud(), (Plugin)this);
        pm.registerEvents((Listener)new MagicHelix(), (Plugin)this);
        pm.registerEvents((Listener)new LavaDrip(), (Plugin)this);
        pm.registerEvents((Listener)new VoidFog(), (Plugin)this);
        pm.registerEvents((Listener)new ExitMenu(), (Plugin)this);
        pm.registerEvents((Listener)new CancelParticle(), (Plugin)this);
        pm.registerEvents((Listener)new SignGUI(), (Plugin)this);
        pm.registerEvents((Listener)new Quit(), (Plugin)this);
    }
    
    private void registerCommands() {
        this.getCommand("effectmenu").setExecutor((CommandExecutor)new EffectCommand());
        this.getCommand("effecthelp").setExecutor((CommandExecutor)new EffectHelp());
        this.getCommand("effectsign").setExecutor((CommandExecutor)new EffectSign());
    }
    
    private void registerConfig() {
        this.getConfig().options().copyDefaults(true);
        this.saveConfig();
    }
    
    public static String Format(final String text) {
        return text.replaceAll("&", "&");
    }
    
    public static ItemStack createItem(final Material mat, final String displayName) {
        final ItemStack item = new ItemStack(mat);
        final ItemMeta iMeta = item.getItemMeta();
        iMeta.getDisplayName();
        iMeta.setDisplayName(displayName);
        item.setItemMeta(iMeta);
        return item;
    }
    
    public static ItemStack glassPane(final Material matt, final int i, final byte b, final String panename) {
        final ItemStack pane = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)14);
        final ItemMeta iMeta2 = pane.getItemMeta();
        iMeta2.setDisplayName(panename);
        pane.setItemMeta(iMeta2);
        return pane;
    }
}